//
//  HospitalDetailsViewModel.swift
//  Hospitals
//
//  Created by Konstantinos Angistalis on 23/09/2017.
//  Copyright © 2017 Konstantinos Angistalis. All rights reserved.
//

import Foundation
import MapKit


class HospitalDetailsViewModel: NSObject {
    
    private let hospital: Hospital!
    
    
    required init?(_ hospital: Hospital?) {
        
        guard let hospital = hospital else {
            return nil
        }
        
        self.hospital = hospital
        
        super.init()
    }
    
    func hospitalName() -> String {
        return hospital.name
    }
    
    func hospitalLocation() -> String? {
        return "\(hospital.city ?? ""), \(hospital.county ?? ""), \(hospital.postcode ?? "")"
    }
    
    func hospitalDescription() -> String? {
        
        return "\(hospital.phone ?? "No phone available")\n\(hospital.website ?? "No email available")\n\(hospital.email ?? "No website available")\n\(hospital.fax ?? "No fax available")"
    }
    
    func hospitalPin() -> MKPointAnnotation? {
        
        if let latitude = hospital.latitude, let longitude = hospital.longitude {
            let annotation = MKPointAnnotation()
            let centerCoordinate = CLLocationCoordinate2D(latitude: latitude, longitude:longitude)
            annotation.coordinate = centerCoordinate
            return annotation
        } else {
            return nil
        }
    }
}
