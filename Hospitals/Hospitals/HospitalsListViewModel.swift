//
//  HospitalsListViewModel.swift
//  Hospitals
//
//  Created by Konstantinos Angistalis on 23/09/2017.
//  Copyright © 2017 Konstantinos Angistalis. All rights reserved.
//

import Foundation


class HospitalsListViewModel: NSObject {
    
    fileprivate var hospitals: [Hospital]?
    fileprivate var dataTask: URLSessionTask?
    
    fileprivate var filter: FilterViewModel?
    fileprivate var filteredHospitals: [Hospital]?
    
    
    deinit {
        debugPrint("HospitalsListViewModel deinited")
        dataTask?.cancel()
    }
    
    /// Retutn the appropriate array in case the filter is active
    fileprivate func targetArray() -> [Hospital]? {
        
        if filter?.selectedOption != nil {
            return filteredHospitals
        } else {
            return hospitals
        }
    }
    
    /// Refresh all hospitals
    func refreshHospitals(completion: @escaping (Error?) -> Void) {
        
        dataTask = NetworkClient.getHospitals { [weak self] (response) in
            
            DispatchQueue.main.async {
                
                guard let blockSelf = self else {
                    return
                }
                
                switch response {
                case .success(let hospitals):
                    blockSelf.hospitals = hospitals
                    
                    // Calcutlate the filter options
                    var options = Set<String>()
                    _ = hospitals.map { options.insert($0.sector) }
                    
                    let filter = FilterViewModel(with: Array(options))
                    filter.selectedOption = blockSelf.filter?.selectedOption
                    
                    // Reapply the filter to go through the new incoming hospitals
                    blockSelf.update(filter: filter)
                    completion(nil)
                    
                case .error(let error):
                    // Show error
                    completion(error)
                }
                
                blockSelf.dataTask = nil
            }
        }
    }
    
    func numberOfHospitals() -> Int {
        return targetArray()?.count ?? 0
    }
    
    func hospitalDetails(at indexPath: IndexPath) -> HospitalDetailsViewModel? {
        
        // Make sure we are not of of bounds of the array
        guard let hospitals = targetArray(), indexPath.row < hospitals.count else {
            return nil
        }
        
        return HospitalDetailsViewModel(hospitals[indexPath.row]) 
    }
}


// MARK: - Filter Methods

extension HospitalsListViewModel {
    
    func currentFilter() -> FilterViewModel? {
        return filter
    }
    
    func update(filter viewModel: FilterViewModel) {
        filter = viewModel
        
        if let selection = viewModel.selectedOption {
            // Filter the available hospitals
            filteredHospitals = hospitals?.filter { $0.sector == selection }
        } else {
            filteredHospitals = nil
        }
    }
}


// MARK: - Decorations Methods

extension HospitalsListViewModel: TableViewDecorating {
    
    func title(at indexPath: IndexPath) -> String? {
        
        // Make sure we are not of of bounds of the array
        guard let hospitals = targetArray(), indexPath.row < hospitals.count else {
            return nil
        }
        
        return hospitals[indexPath.row].name
    }
    
    func subtitle(at indexPath: IndexPath) -> String? {
        
        // Make sure we are not of of bounds of the array
        guard let hospitals = targetArray(), indexPath.row < hospitals.count else {
            return nil
        }
        
        let hospital = hospitals[indexPath.row]
        return "\(hospital.sector), \(hospital.city ?? ""), \(hospital.county ?? "")"
    }
}
