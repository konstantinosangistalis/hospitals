//
//  NetworkClient.swift
//  Hospitals
//
//  Created by Konstantinos Angistalis on 23/09/2017.
//  Copyright © 2017 Konstantinos Angistalis. All rights reserved.
//

import Foundation

import os.log


/// API error related constants
public enum APIErrorConstants {
    static let domain = "com.angistalis.Hospitals.errorDomain"
    static let parsingError = -801
    static let baseURLError = -802
    static let csvFormatError = -803
    static let emptyResponseError = -804
}

/// Used for passing data for presentation. localisedMessage can be used to pass user facing error messages
public enum NetworkResponse<T> {
    case success(T)
    case error(Error)
}


public class NetworkClient {
    
    private static let log = OSLog(subsystem: "com.angistalis.Hospitals", category: "Network")


    // MARK: API Methods
    
    /// Get all hospitals.
    static func getHospitals(completion: ((NetworkResponse<[Hospital]>) -> Void)? = nil ) -> URLSessionDataTask? {
        
        // Chec the baseURL
        guard let url = URL(string: "https://data.gov.uk/data/resource/nhschoices/Hospital.csv") else {
            os_log("Invalid Base URL", log: NetworkClient.log, type: .error, "")
            completion?(NetworkResponse.error(NSError(domain: APIErrorConstants.domain,
                                                      code: APIErrorConstants.baseURLError,
                                                      userInfo: nil)))
            return nil
        }
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: url) { (data, response, error) in
            
            if let error = error {
                os_log("Hospital.csv network request error: %@", log: NetworkClient.log, type: .error, error.localizedDescription)
                completion?(NetworkResponse.error(error))
                
            } else if let data = data {
                
                if let responseString = NSString(data: data, encoding: String.Encoding.ascii.rawValue) {
                    
                    // Read all the lines of the file
                    var lines = responseString.components(separatedBy: "\r\n")
                    
                    guard lines.count > 0 else {
                        os_log("Hospital.csv has no columns", log: NetworkClient.log, type: .error, "")
                        completion?(NetworkResponse.error(NSError(domain: APIErrorConstants.domain,
                                                                  code: APIErrorConstants.csvFormatError,
                                                                  userInfo: nil)))
                        return
                    }
                    
                    // Remove the header with the column names
                    let columns = lines.removeFirst().components(separatedBy: "\t")
                    
                    var result = [Hospital]()
                    
                    for line in lines {
                        // For each line then create a dictionary with keys the names of the columns and values the stings of the line
                        let lineArray = line.components(separatedBy: "\t")
                        
                        if lineArray.count == columns.count {
                            
                            // Parse the hospital object and add it in the result array
                            do {
                                try result.append(Hospital(with: NSDictionary(objects: lineArray, forKeys: columns as [NSCopying])))
                            } catch (let error) {
                                os_log("Failed to parse mandatory property", log: NetworkClient.log, type: .info, error.localizedDescription)
                            }
                            
                        } else if lineArray.count > 1 {
                            debugPrint("Invalid line: \(line)")
                        }
                    }
                    
                    // Call the completion block with the results
                    completion?(NetworkResponse.success(result))
                    
                } else {
                    os_log("Failed to parse Hospital.csv", log: NetworkClient.log, type: .error, "")
                    completion?(NetworkResponse.error(NSError(domain: APIErrorConstants.domain,
                                                              code: APIErrorConstants.csvFormatError,
                                                              userInfo: nil)))
                }
                
            } else {
                os_log("Hospital.csv empty request", log: NetworkClient.log, type: .error, "")
                completion?(NetworkResponse.error(NSError(domain: APIErrorConstants.domain,
                                                          code: APIErrorConstants.emptyResponseError,
                                                          userInfo: nil)))
            }
        }
        
        dataTask.resume()
        
        // Return the task in case the owner needs to cancel it
        return dataTask
    }
}
