//
//  Hospital.swift
//  Hospitals
//
//  Created by Konstantinos Angistalis on 23/09/2017.
//  Copyright © 2017 Konstantinos Angistalis. All rights reserved.
//

import Foundation


/// Contains information on what was went wrong when parsing a hospital row from the CSV file.
struct CSVParsingError: Error {
    enum ErrorKind {
        case invalidMandatoryProperty
    }
    
    let column: String
    let kind: ErrorKind
    
    var localizedDescription: String {
        switch kind {
        case .invalidMandatoryProperty:
            return "Invalid value for column: \(column)"
        }
    }
}


struct Hospital {
    
    let id: String
    let code: String
    let organisationType: String
    let organisationSubType: String
    let sector: String
    let status: String
    let name: String
    let isPimsManaged: Bool
    
    let addressLine1: String?
    let addressLine2: String?
    let addressLine3: String?
    
    let city: String?
    let county: String?
    let postcode: String?
    
    let latitude: Double?
    let longitude: Double?
    
    let parentODSCode: String?
    let parentName: String?
    
    let phone: String?
    let email: String?
    let website: String?
    let fax: String?
    
    
    init(with dictionary: NSDictionary) throws {
        
        // Mandatory Proporties
        if let id = dictionary["OrganisationID"] as? String {
            self.id = id
        } else {
            throw CSVParsingError(column: "OrganisationID", kind: .invalidMandatoryProperty)
        }
        
        if let code = dictionary["OrganisationCode"] as? String {
            self.code = code
        } else {
            throw CSVParsingError(column: "OrganisationCode", kind: .invalidMandatoryProperty)
        }
        
        if let organisationType = dictionary["OrganisationType"] as? String {
            self.organisationType = organisationType
        } else {
            throw CSVParsingError(column: "OrganisationType", kind: .invalidMandatoryProperty)
        }
        
        if let organisationSubType = dictionary["SubType"] as? String {
            self.organisationSubType = organisationSubType
        } else {
            throw CSVParsingError(column: "SubType", kind: .invalidMandatoryProperty)
        }
        
        if let sector = dictionary["Sector"] as? String {
            self.sector = sector
        } else {
            throw CSVParsingError(column: "Sector", kind: .invalidMandatoryProperty)
        }
        
        if let status = dictionary["OrganisationStatus"] as? String {
            self.status = status
        } else {
            throw CSVParsingError(column: "OrganisationStatus", kind: .invalidMandatoryProperty)
        }
        
        if let name = dictionary["OrganisationName"] as? String {
            self.name = name
        } else {
            throw CSVParsingError(column: "OrganisationName", kind: .invalidMandatoryProperty)
        }
        
        if let isPimsManaged = dictionary["IsPimsManaged"] as? String {
            self.isPimsManaged = isPimsManaged == "True"
        } else {
            throw CSVParsingError(column: "IsPimsManaged", kind: .invalidMandatoryProperty)
        }
        
        
        // Optional Values

        if let longitude = dictionary["Longitude"] as? String, let doubleValue = Double(longitude) {
            self.longitude = doubleValue
        } else {
            self.longitude = nil
        }
        
        if let latitude = dictionary["Latitude"] as? String, let doubleValue = Double(latitude) {
            self.latitude = doubleValue
        } else {
            self.latitude = nil
        }
        
        self.addressLine1 = dictionary["Address1"] as? String
        self.addressLine2 = dictionary["Address2"] as? String
        self.addressLine3 = dictionary["Address3"] as? String
        self.city = dictionary["City"] as? String
        self.county = dictionary["County"] as? String
        self.postcode = dictionary["Postcode"] as? String
        self.parentODSCode = dictionary["ParentODSCode"] as? String
        self.parentName = dictionary["ParentName"] as? String
        self.phone = dictionary["Phone"] as? String
        self.email = dictionary["Email"] as? String
        self.website = dictionary["Website"] as? String
        self.fax = dictionary["Fax"] as? String
    }
}
