//
//  DetailViewController.swift
//  Hospitals
//
//  Created by Konstantinos Angistalis on 23/09/2017.
//  Copyright © 2017 Konstantinos Angistalis. All rights reserved.
//

import UIKit
import MapKit


class DetailViewController: UIViewController {
    
    @IBOutlet weak var ibNameLabel: UILabel!
    @IBOutlet weak var ibMapView: MKMapView!
    @IBOutlet weak var ibLocationLabel: UILabel!
    @IBOutlet weak var ibDescriptionLabel: UILabel!
    
    var viewModel: HospitalDetailsViewModel? {
        didSet {
            // Update the view.
            self.loadViewIfNeeded()
            configureView()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configureView() {
        // Update the user interface for the detail item.
        ibNameLabel.text = viewModel?.hospitalName()
        ibLocationLabel.text = viewModel?.hospitalLocation()
        ibDescriptionLabel.text = viewModel?.hospitalDescription()
        
        ibMapView.removeAnnotations(ibMapView.annotations)
        if let pin = viewModel?.hospitalPin() {
            ibMapView.isHidden = false
            ibMapView.addAnnotation(pin)
            
            // Zoom to pin
            let span = MKCoordinateSpanMake(0.5, 0.5)
            let region = MKCoordinateRegion(center: pin.coordinate, span: span)
            ibMapView.setRegion(region, animated: false)
        } else {
            ibMapView.isHidden = true
        }
    }
}
