//
//  FilterViewModel.swift
//  Hospitals
//
//  Created by Konstantinos Angistalis on 24/09/2017.
//  Copyright © 2017 Konstantinos Angistalis. All rights reserved.
//

import UIKit

class FilterViewModel: NSObject {

    fileprivate var options: [String]?
    var selectedOption: String?
    
    init(with options: [String]?) {
        self.options = options
    }
    
    deinit {
        debugPrint("FilterViewModel deinited")
    }
    
    func numberOfOptions() -> Int {
        return options?.count ?? 0
    }
    
    func updateSelection(at indexPath: IndexPath?) {
        
        guard let indexPath = indexPath else {
            selectedOption = nil
            return
        }
        
        // Make sure we are not of of bounds of the array
        guard let options = options, indexPath.row < options.count else {
            return
        }
        
        selectedOption = options[indexPath.row]
    }
}

extension FilterViewModel: TableViewDecorating {
    
    func title(at indexPath: IndexPath) -> String? {
        
        // Make sure we are not of of bounds of the array
        guard let options = options, indexPath.row < options.count else {
            return nil
        }
        
        return options[indexPath.row]
    }
    
    func subtitle(at indexPath: IndexPath) -> String? {
        return nil
    }
}

extension FilterViewModel: TableViewSelecting {
    
    func isSelected(at indexPath: IndexPath) -> Bool {
        
        // Make sure we are not of of bounds of the array
        guard let options = options, indexPath.row < options.count else {
            return false
        }
        
        return options[indexPath.row] == selectedOption
    }
}
