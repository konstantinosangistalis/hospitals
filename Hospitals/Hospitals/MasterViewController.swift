//
//  MasterViewController.swift
//  Hospitals
//
//  Created by Konstantinos Angistalis on 23/09/2017.
//  Copyright © 2017 Konstantinos Angistalis. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController {

    private var viewModel = HospitalsListViewModel()


    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup the UIRefreshControl
        refreshControl = UIRefreshControl()
        
        if let refreshControl = refreshControl {
            refreshControl.addTarget(self, action: #selector(MasterViewController.refreshAction(sender:)), for: .valueChanged)
            
            // Perform the initial load
            refreshControl.beginRefreshing()
            refreshAction(sender: refreshControl)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Refresh
    
    func refreshAction(sender: UIRefreshControl) {
        
        viewModel.refreshHospitals { [weak self] (error) in
            
            guard let blockSelf = self else {
                return
            }
            
            blockSelf.refreshControl?.endRefreshing()
            
            if let error = error {
                // Show error
                let alertController = UIAlertController(title: "Generic Error", message: error.localizedDescription, preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                blockSelf.present(alertController, animated: true, completion: nil)
            } else {
                blockSelf.tableView.reloadData()
            }
        }
    }
    
    
    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow,
                let controller = (segue.destination as! UINavigationController).topViewController as? DetailViewController,
                let detailsViewModel = viewModel.hospitalDetails(at: indexPath) {
                
                controller.viewModel = detailsViewModel
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
            
        } else if segue.identifier == "showFilters" {
            if let controller = (segue.destination as! UINavigationController).topViewController as? FilterTableViewController,
                let filterViewModel = viewModel.currentFilter() {
                
                controller.viewModel = filterViewModel
                
                controller.selectionClosure = { [weak self] () in
                    self?.viewModel.update(filter: controller.viewModel)
                    self?.tableView.reloadData()
                }
            }
        }
    }

    
    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfHospitals()
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        cell.textLabel?.text = viewModel.title(at: indexPath)
        cell.detailTextLabel?.text = viewModel.subtitle(at: indexPath)
        
        return cell
    }
}

