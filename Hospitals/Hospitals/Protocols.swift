//
//  Protocols.swift
//  Hospitals
//
//  Created by Konstantinos Angistalis on 24/09/2017.
//  Copyright © 2017 Konstantinos Angistalis. All rights reserved.
//

import Foundation

protocol TableViewDecorating {
    func title(at indexPath: IndexPath) -> String?
    func subtitle(at indexPath: IndexPath) -> String?
}

protocol TableViewSelecting {
    func isSelected(at indexPath: IndexPath) -> Bool
}

