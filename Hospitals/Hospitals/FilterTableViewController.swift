//
//  FilterTableViewController.swift
//  Hospitals
//
//  Created by Konstantinos Angistalis on 24/09/2017.
//  Copyright © 2017 Konstantinos Angistalis. All rights reserved.
//

import UIKit

class FilterTableViewController: UITableViewController {

    var viewModel: FilterViewModel!
    
    var selectionClosure: ((Void) -> Void)?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func cancelButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clearButtonTapped(_ sender: Any) {
        viewModel.updateSelection(at: nil)
        selectionClosure?()
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return viewModel.numberOfOptions()
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilterCell", for: indexPath)

        // Configure the cell...
        cell.textLabel?.text = viewModel.title(at: indexPath)
        
        if viewModel.isSelected(at: indexPath) {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        viewModel.updateSelection(at: indexPath)
        selectionClosure?()
        dismiss(animated: true, completion: nil)
    }
}
